package smir.control

import groovy.json.JsonSlurper
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import smir.model.Item

/**
 * Created by palmund on 13/05/14.
 */
class WebController {
    private final String baseUrl = "http://localhost:9200/"
    private final String index = "fridge"
    private final CloseableHttpClient client = HttpClients.createDefault()

    Collection<Item> find(String query) {
        return advancedFind("name:*${query}* OR tag:${query}")
    }

    Collection<Item> advancedFind(String query) {
        def path = makePath(query)
        def request = new HttpGet(path)
        def response = client.execute(request)
        def json = convertRawToJson(response)

        def results = json["hits"]["hits"]

        def list = []
        results.each {
            list << new Item(name: it["_source"].name, expires: it["_source"].expires)
        }
        return list
    }

    private static def convertRawToJson(CloseableHttpResponse response) {
        def raw = EntityUtils.toString(response.entity)
        def json = new JsonSlurper().parseText(raw)
        json
    }

    private String makePath(String query) {
        query = query.replace('>', '%3E')
                     .replace(' ', '%20')

        baseUrl + index + "/_search?q=" + query
    }
}