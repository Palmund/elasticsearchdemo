package smir.view;

import smir.control.WebController;
import smir.model.Item;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

/**
 * Created by palmund on 13/05/14.
 */
public class FridgeContentView {
    private JPanel panel1;
    private JTextField searchField;
    private JTable fridgeContentTable;
    private JCheckBox advancedSearchCheckBox;

    public FridgeContentView() {
        fridgeContentTable.setModel(new DefaultTableModel(new Object[]{"Navn", "Udløbsdato"}, 0));

        searchField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.println("hej");
                final String query = searchField.getText();
                final Collection<Item> items = performSearch(query);
                addResultsToTable(items);
            }
        });

        final Collection<Item> items = performSearch("*");
        addResultsToTable(items);
        fridgeContentTable.repaint();
    }

    private void addResultsToTable(Collection<Item> results) {
        while (((DefaultTableModel) fridgeContentTable.getModel()).getRowCount() > 0) {
            ((DefaultTableModel) fridgeContentTable.getModel()).removeRow(0);
        }
        for (Object entity : results) {
            ((DefaultTableModel) fridgeContentTable.getModel()).addRow(
                    new Object[]{
                        ((Item) entity).getName(),
                        ((Item) entity).getExpires()
                    }
            );
        }
    }

    private Collection<Item> performSearch(String query) {
        final WebController controller = new WebController();
        if (advancedSearchCheckBox.isSelected()) {
            return controller.advancedFind(query);
        } else {
            return controller.find(query);
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FridgeContentView");
        frame.setContentPane(new FridgeContentView().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}